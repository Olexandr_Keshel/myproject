﻿namespace MyProject
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            this.LoadDishesBtn = new System.Windows.Forms.Button();
            this.dataGridView1 = new System.Windows.Forms.DataGridView();
            this.SearchBtn = new System.Windows.Forms.Button();
            this.searchTxtBox = new System.Windows.Forms.TextBox();
            this.dishPictureBox = new System.Windows.Forms.PictureBox();
            this.addDishGroupBox = new System.Windows.Forms.GroupBox();
            this.dishIdLabel = new System.Windows.Forms.Label();
            this.editCheckBox1 = new System.Windows.Forms.CheckBox();
            this.addOrUpdateDishBtn = new System.Windows.Forms.Button();
            this.ingridientsTxtBox = new System.Windows.Forms.TextBox();
            this.imageUrlTxtBox = new System.Windows.Forms.TextBox();
            this.cookingTimeTxtBox = new System.Windows.Forms.TextBox();
            this.originTxtBox = new System.Windows.Forms.TextBox();
            this.dishNametxtBox = new System.Windows.Forms.TextBox();
            this.ingridientsLabel = new System.Windows.Forms.Label();
            this.imageUrlLabel = new System.Windows.Forms.Label();
            this.cookingTimeLabel = new System.Windows.Forms.Label();
            this.OrginLabel = new System.Windows.Forms.Label();
            this.dishNameLabel = new System.Windows.Forms.Label();
            this.videosLabel = new System.Windows.Forms.Label();
            this.dataGridView2 = new System.Windows.Forms.DataGridView();
            this.deleteVideoBtn = new System.Windows.Forms.Button();
            this.editDishBtn = new System.Windows.Forms.Button();
            this.addVideoGroupBox = new System.Windows.Forms.GroupBox();
            this.videoIdLabel = new System.Windows.Forms.Label();
            this.editCheckBox2 = new System.Windows.Forms.CheckBox();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.addOrUpdateVideoBtn = new System.Windows.Forms.Button();
            this.videoYearTxtBox = new System.Windows.Forms.TextBox();
            this.videoLangTxtBox = new System.Windows.Forms.TextBox();
            this.videoURLTxtBox = new System.Windows.Forms.TextBox();
            this.videoTitleTxtBox = new System.Windows.Forms.TextBox();
            this.videoDishIdLabel = new System.Windows.Forms.Label();
            this.videoYearLabel = new System.Windows.Forms.Label();
            this.videoLangLabel = new System.Windows.Forms.Label();
            this.videoURLLabel = new System.Windows.Forms.Label();
            this.videoTitleLabel = new System.Windows.Forms.Label();
            this.editVideoBtn = new System.Windows.Forms.Button();
            this.deleteDishBtn = new System.Windows.Forms.Button();
            this.webBrowser1 = new System.Windows.Forms.WebBrowser();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dishPictureBox)).BeginInit();
            this.addDishGroupBox.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).BeginInit();
            this.addVideoGroupBox.SuspendLayout();
            this.SuspendLayout();
            // 
            // LoadDishesBtn
            // 
            this.LoadDishesBtn.Location = new System.Drawing.Point(343, 43);
            this.LoadDishesBtn.Name = "LoadDishesBtn";
            this.LoadDishesBtn.Size = new System.Drawing.Size(165, 43);
            this.LoadDishesBtn.TabIndex = 0;
            this.LoadDishesBtn.Text = "Reload ";
            this.LoadDishesBtn.UseVisualStyleBackColor = true;
            this.LoadDishesBtn.Click += new System.EventHandler(this.ReloadDishesBtn_Click);
            // 
            // dataGridView1
            // 
            this.dataGridView1.AllowUserToAddRows = false;
            this.dataGridView1.AllowUserToDeleteRows = false;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle2.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle2.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle2.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle2.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle2.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle2.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView1.DefaultCellStyle = dataGridViewCellStyle2;
            this.dataGridView1.Location = new System.Drawing.Point(343, 150);
            this.dataGridView1.Name = "dataGridView1";
            this.dataGridView1.ReadOnly = true;
            dataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView1.RowHeadersDefaultCellStyle = dataGridViewCellStyle3;
            this.dataGridView1.RowHeadersWidth = 51;
            this.dataGridView1.RowTemplate.Height = 24;
            this.dataGridView1.Size = new System.Drawing.Size(878, 200);
            this.dataGridView1.TabIndex = 1;
            this.dataGridView1.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView1_CellClick);
            // 
            // SearchBtn
            // 
            this.SearchBtn.Location = new System.Drawing.Point(972, 46);
            this.SearchBtn.Name = "SearchBtn";
            this.SearchBtn.Size = new System.Drawing.Size(160, 40);
            this.SearchBtn.TabIndex = 2;
            this.SearchBtn.Text = "Search";
            this.SearchBtn.UseVisualStyleBackColor = true;
            this.SearchBtn.Click += new System.EventHandler(this.SearchBtn_Click);
            // 
            // searchTxtBox
            // 
            this.searchTxtBox.Location = new System.Drawing.Point(538, 55);
            this.searchTxtBox.Name = "searchTxtBox";
            this.searchTxtBox.Size = new System.Drawing.Size(428, 22);
            this.searchTxtBox.TabIndex = 3;
            // 
            // dishPictureBox
            // 
            this.dishPictureBox.Location = new System.Drawing.Point(1312, 136);
            this.dishPictureBox.Name = "dishPictureBox";
            this.dishPictureBox.Size = new System.Drawing.Size(249, 236);
            this.dishPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.dishPictureBox.TabIndex = 4;
            this.dishPictureBox.TabStop = false;
            // 
            // addDishGroupBox
            // 
            this.addDishGroupBox.Controls.Add(this.dishIdLabel);
            this.addDishGroupBox.Controls.Add(this.editCheckBox1);
            this.addDishGroupBox.Controls.Add(this.addOrUpdateDishBtn);
            this.addDishGroupBox.Controls.Add(this.ingridientsTxtBox);
            this.addDishGroupBox.Controls.Add(this.imageUrlTxtBox);
            this.addDishGroupBox.Controls.Add(this.cookingTimeTxtBox);
            this.addDishGroupBox.Controls.Add(this.originTxtBox);
            this.addDishGroupBox.Controls.Add(this.dishNametxtBox);
            this.addDishGroupBox.Controls.Add(this.ingridientsLabel);
            this.addDishGroupBox.Controls.Add(this.imageUrlLabel);
            this.addDishGroupBox.Controls.Add(this.cookingTimeLabel);
            this.addDishGroupBox.Controls.Add(this.OrginLabel);
            this.addDishGroupBox.Controls.Add(this.dishNameLabel);
            this.addDishGroupBox.Location = new System.Drawing.Point(12, 43);
            this.addDishGroupBox.Name = "addDishGroupBox";
            this.addDishGroupBox.Size = new System.Drawing.Size(284, 307);
            this.addDishGroupBox.TabIndex = 5;
            this.addDishGroupBox.TabStop = false;
            this.addDishGroupBox.Text = "Add Dish";
            // 
            // dishIdLabel
            // 
            this.dishIdLabel.AutoSize = true;
            this.dishIdLabel.Location = new System.Drawing.Point(196, 254);
            this.dishIdLabel.Name = "dishIdLabel";
            this.dishIdLabel.Size = new System.Drawing.Size(48, 16);
            this.dishIdLabel.TabIndex = 9;
            this.dishIdLabel.Text = "dish ID";
            // 
            // editCheckBox1
            // 
            this.editCheckBox1.AutoCheck = false;
            this.editCheckBox1.AutoSize = true;
            this.editCheckBox1.Enabled = false;
            this.editCheckBox1.ForeColor = System.Drawing.Color.Black;
            this.editCheckBox1.Location = new System.Drawing.Point(129, 253);
            this.editCheckBox1.Name = "editCheckBox1";
            this.editCheckBox1.Size = new System.Drawing.Size(51, 20);
            this.editCheckBox1.TabIndex = 8;
            this.editCheckBox1.Text = "edit";
            this.editCheckBox1.UseVisualStyleBackColor = true;
            // 
            // addOrUpdateDishBtn
            // 
            this.addOrUpdateDishBtn.Location = new System.Drawing.Point(4, 246);
            this.addOrUpdateDishBtn.Name = "addOrUpdateDishBtn";
            this.addOrUpdateDishBtn.Size = new System.Drawing.Size(108, 29);
            this.addOrUpdateDishBtn.TabIndex = 7;
            this.addOrUpdateDishBtn.Text = "Add/Update";
            this.addOrUpdateDishBtn.UseVisualStyleBackColor = true;
            this.addOrUpdateDishBtn.Click += new System.EventHandler(this.addOrUpdateDishBtn_Click);
            // 
            // ingridientsTxtBox
            // 
            this.ingridientsTxtBox.Location = new System.Drawing.Point(107, 201);
            this.ingridientsTxtBox.Name = "ingridientsTxtBox";
            this.ingridientsTxtBox.Size = new System.Drawing.Size(144, 22);
            this.ingridientsTxtBox.TabIndex = 6;
            // 
            // imageUrlTxtBox
            // 
            this.imageUrlTxtBox.Location = new System.Drawing.Point(107, 156);
            this.imageUrlTxtBox.Name = "imageUrlTxtBox";
            this.imageUrlTxtBox.Size = new System.Drawing.Size(144, 22);
            this.imageUrlTxtBox.TabIndex = 6;
            // 
            // cookingTimeTxtBox
            // 
            this.cookingTimeTxtBox.Location = new System.Drawing.Point(107, 111);
            this.cookingTimeTxtBox.Name = "cookingTimeTxtBox";
            this.cookingTimeTxtBox.Size = new System.Drawing.Size(144, 22);
            this.cookingTimeTxtBox.TabIndex = 6;
            // 
            // originTxtBox
            // 
            this.originTxtBox.Location = new System.Drawing.Point(107, 66);
            this.originTxtBox.Name = "originTxtBox";
            this.originTxtBox.Size = new System.Drawing.Size(144, 22);
            this.originTxtBox.TabIndex = 6;
            // 
            // dishNametxtBox
            // 
            this.dishNametxtBox.Location = new System.Drawing.Point(107, 21);
            this.dishNametxtBox.Name = "dishNametxtBox";
            this.dishNametxtBox.Size = new System.Drawing.Size(144, 22);
            this.dishNametxtBox.TabIndex = 6;
            // 
            // ingridientsLabel
            // 
            this.ingridientsLabel.AutoSize = true;
            this.ingridientsLabel.Location = new System.Drawing.Point(12, 203);
            this.ingridientsLabel.Name = "ingridientsLabel";
            this.ingridientsLabel.Size = new System.Drawing.Size(68, 16);
            this.ingridientsLabel.TabIndex = 5;
            this.ingridientsLabel.Text = "Ingridients";
            // 
            // imageUrlLabel
            // 
            this.imageUrlLabel.AutoSize = true;
            this.imageUrlLabel.Location = new System.Drawing.Point(12, 159);
            this.imageUrlLabel.Name = "imageUrlLabel";
            this.imageUrlLabel.Size = new System.Drawing.Size(75, 16);
            this.imageUrlLabel.TabIndex = 4;
            this.imageUrlLabel.Text = "Image URL";
            // 
            // cookingTimeLabel
            // 
            this.cookingTimeLabel.AutoSize = true;
            this.cookingTimeLabel.Location = new System.Drawing.Point(12, 115);
            this.cookingTimeLabel.Name = "cookingTimeLabel";
            this.cookingTimeLabel.Size = new System.Drawing.Size(91, 16);
            this.cookingTimeLabel.TabIndex = 3;
            this.cookingTimeLabel.Text = "Cooking Time";
            // 
            // OrginLabel
            // 
            this.OrginLabel.AutoSize = true;
            this.OrginLabel.Location = new System.Drawing.Point(12, 71);
            this.OrginLabel.Name = "OrginLabel";
            this.OrginLabel.Size = new System.Drawing.Size(42, 16);
            this.OrginLabel.TabIndex = 1;
            this.OrginLabel.Text = "Origin";
            // 
            // dishNameLabel
            // 
            this.dishNameLabel.AutoSize = true;
            this.dishNameLabel.Location = new System.Drawing.Point(12, 27);
            this.dishNameLabel.Name = "dishNameLabel";
            this.dishNameLabel.Size = new System.Drawing.Size(74, 16);
            this.dishNameLabel.TabIndex = 0;
            this.dishNameLabel.Text = "Dish Name";
            // 
            // videosLabel
            // 
            this.videosLabel.AutoSize = true;
            this.videosLabel.Location = new System.Drawing.Point(495, 374);
            this.videosLabel.Name = "videosLabel";
            this.videosLabel.Size = new System.Drawing.Size(50, 16);
            this.videosLabel.TabIndex = 6;
            this.videosLabel.Text = "Videos";
            // 
            // dataGridView2
            // 
            this.dataGridView2.AllowUserToAddRows = false;
            this.dataGridView2.AllowUserToDeleteRows = false;
            dataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle4;
            this.dataGridView2.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            dataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window;
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText;
            dataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.False;
            this.dataGridView2.DefaultCellStyle = dataGridViewCellStyle5;
            this.dataGridView2.Location = new System.Drawing.Point(343, 489);
            this.dataGridView2.Name = "dataGridView2";
            this.dataGridView2.ReadOnly = true;
            dataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            dataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dataGridView2.RowHeadersDefaultCellStyle = dataGridViewCellStyle6;
            this.dataGridView2.RowHeadersWidth = 51;
            this.dataGridView2.RowTemplate.Height = 24;
            this.dataGridView2.Size = new System.Drawing.Size(878, 180);
            this.dataGridView2.TabIndex = 1;
            this.dataGridView2.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dataGridView2_CellClick);
            // 
            // deleteVideoBtn
            // 
            this.deleteVideoBtn.Location = new System.Drawing.Point(343, 702);
            this.deleteVideoBtn.Name = "deleteVideoBtn";
            this.deleteVideoBtn.Size = new System.Drawing.Size(178, 36);
            this.deleteVideoBtn.TabIndex = 7;
            this.deleteVideoBtn.Text = "Delete Selected Video";
            this.deleteVideoBtn.UseVisualStyleBackColor = true;
            this.deleteVideoBtn.Click += new System.EventHandler(this.deleteVideoBtn_Click);
            // 
            // editDishBtn
            // 
            this.editDishBtn.Location = new System.Drawing.Point(1138, 46);
            this.editDishBtn.Name = "editDishBtn";
            this.editDishBtn.Size = new System.Drawing.Size(126, 43);
            this.editDishBtn.TabIndex = 8;
            this.editDishBtn.Text = "Edit selected Dish";
            this.editDishBtn.UseVisualStyleBackColor = true;
            this.editDishBtn.Click += new System.EventHandler(this.editDishBtn_Click);
            // 
            // addVideoGroupBox
            // 
            this.addVideoGroupBox.Controls.Add(this.videoIdLabel);
            this.addVideoGroupBox.Controls.Add(this.editCheckBox2);
            this.addVideoGroupBox.Controls.Add(this.comboBox1);
            this.addVideoGroupBox.Controls.Add(this.addOrUpdateVideoBtn);
            this.addVideoGroupBox.Controls.Add(this.videoYearTxtBox);
            this.addVideoGroupBox.Controls.Add(this.videoLangTxtBox);
            this.addVideoGroupBox.Controls.Add(this.videoURLTxtBox);
            this.addVideoGroupBox.Controls.Add(this.videoTitleTxtBox);
            this.addVideoGroupBox.Controls.Add(this.videoDishIdLabel);
            this.addVideoGroupBox.Controls.Add(this.videoYearLabel);
            this.addVideoGroupBox.Controls.Add(this.videoLangLabel);
            this.addVideoGroupBox.Controls.Add(this.videoURLLabel);
            this.addVideoGroupBox.Controls.Add(this.videoTitleLabel);
            this.addVideoGroupBox.Location = new System.Drawing.Point(12, 422);
            this.addVideoGroupBox.Name = "addVideoGroupBox";
            this.addVideoGroupBox.Size = new System.Drawing.Size(307, 346);
            this.addVideoGroupBox.TabIndex = 9;
            this.addVideoGroupBox.TabStop = false;
            this.addVideoGroupBox.Text = "Add Video";
            // 
            // videoIdLabel
            // 
            this.videoIdLabel.AutoSize = true;
            this.videoIdLabel.Location = new System.Drawing.Point(211, 299);
            this.videoIdLabel.Name = "videoIdLabel";
            this.videoIdLabel.Size = new System.Drawing.Size(57, 16);
            this.videoIdLabel.TabIndex = 21;
            this.videoIdLabel.Text = "video ID";
            // 
            // editCheckBox2
            // 
            this.editCheckBox2.AutoCheck = false;
            this.editCheckBox2.AutoSize = true;
            this.editCheckBox2.Enabled = false;
            this.editCheckBox2.ForeColor = System.Drawing.Color.Black;
            this.editCheckBox2.Location = new System.Drawing.Point(145, 296);
            this.editCheckBox2.Name = "editCheckBox2";
            this.editCheckBox2.Size = new System.Drawing.Size(51, 20);
            this.editCheckBox2.TabIndex = 20;
            this.editCheckBox2.Text = "edit";
            this.editCheckBox2.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(136, 223);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(144, 24);
            this.comboBox1.TabIndex = 11;
            // 
            // addOrUpdateVideoBtn
            // 
            this.addOrUpdateVideoBtn.Location = new System.Drawing.Point(11, 280);
            this.addOrUpdateVideoBtn.Name = "addOrUpdateVideoBtn";
            this.addOrUpdateVideoBtn.Size = new System.Drawing.Size(114, 35);
            this.addOrUpdateVideoBtn.TabIndex = 19;
            this.addOrUpdateVideoBtn.Text = "Add/Update";
            this.addOrUpdateVideoBtn.UseVisualStyleBackColor = true;
            this.addOrUpdateVideoBtn.Click += new System.EventHandler(this.addOrUpdateVideoBtn_Click);
            // 
            // videoYearTxtBox
            // 
            this.videoYearTxtBox.Location = new System.Drawing.Point(136, 179);
            this.videoYearTxtBox.Name = "videoYearTxtBox";
            this.videoYearTxtBox.Size = new System.Drawing.Size(144, 22);
            this.videoYearTxtBox.TabIndex = 12;
            // 
            // videoLangTxtBox
            // 
            this.videoLangTxtBox.Location = new System.Drawing.Point(136, 135);
            this.videoLangTxtBox.Name = "videoLangTxtBox";
            this.videoLangTxtBox.Size = new System.Drawing.Size(144, 22);
            this.videoLangTxtBox.TabIndex = 14;
            // 
            // videoURLTxtBox
            // 
            this.videoURLTxtBox.Location = new System.Drawing.Point(136, 91);
            this.videoURLTxtBox.Name = "videoURLTxtBox";
            this.videoURLTxtBox.Size = new System.Drawing.Size(144, 22);
            this.videoURLTxtBox.TabIndex = 15;
            // 
            // videoTitleTxtBox
            // 
            this.videoTitleTxtBox.Location = new System.Drawing.Point(136, 47);
            this.videoTitleTxtBox.Name = "videoTitleTxtBox";
            this.videoTitleTxtBox.Size = new System.Drawing.Size(144, 22);
            this.videoTitleTxtBox.TabIndex = 16;
            // 
            // videoDishIdLabel
            // 
            this.videoDishIdLabel.AutoSize = true;
            this.videoDishIdLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.videoDishIdLabel.Location = new System.Drawing.Point(12, 223);
            this.videoDishIdLabel.Name = "videoDishIdLabel";
            this.videoDishIdLabel.Size = new System.Drawing.Size(56, 18);
            this.videoDishIdLabel.TabIndex = 10;
            this.videoDishIdLabel.Text = "Dish ID";
            // 
            // videoYearLabel
            // 
            this.videoYearLabel.AutoSize = true;
            this.videoYearLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.videoYearLabel.Location = new System.Drawing.Point(12, 180);
            this.videoYearLabel.Name = "videoYearLabel";
            this.videoYearLabel.Size = new System.Drawing.Size(77, 18);
            this.videoYearLabel.TabIndex = 9;
            this.videoYearLabel.Text = "Video year";
            // 
            // videoLangLabel
            // 
            this.videoLangLabel.AutoSize = true;
            this.videoLangLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.videoLangLabel.Location = new System.Drawing.Point(12, 137);
            this.videoLangLabel.Name = "videoLangLabel";
            this.videoLangLabel.Size = new System.Drawing.Size(113, 18);
            this.videoLangLabel.TabIndex = 8;
            this.videoLangLabel.Text = "Video Language";
            // 
            // videoURLLabel
            // 
            this.videoURLLabel.AutoSize = true;
            this.videoURLLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.videoURLLabel.Location = new System.Drawing.Point(12, 94);
            this.videoURLLabel.Name = "videoURLLabel";
            this.videoURLLabel.Size = new System.Drawing.Size(75, 18);
            this.videoURLLabel.TabIndex = 7;
            this.videoURLLabel.Text = "VideoURL";
            // 
            // videoTitleLabel
            // 
            this.videoTitleLabel.AutoSize = true;
            this.videoTitleLabel.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.videoTitleLabel.Location = new System.Drawing.Point(12, 51);
            this.videoTitleLabel.Name = "videoTitleLabel";
            this.videoTitleLabel.Size = new System.Drawing.Size(71, 18);
            this.videoTitleLabel.TabIndex = 6;
            this.videoTitleLabel.Text = "Video title";
            // 
            // editVideoBtn
            // 
            this.editVideoBtn.Location = new System.Drawing.Point(561, 702);
            this.editVideoBtn.Name = "editVideoBtn";
            this.editVideoBtn.Size = new System.Drawing.Size(159, 36);
            this.editVideoBtn.TabIndex = 10;
            this.editVideoBtn.Text = "Edit selected video";
            this.editVideoBtn.UseVisualStyleBackColor = true;
            this.editVideoBtn.Click += new System.EventHandler(this.editVideoBtn_Click);
            // 
            // deleteDishBtn
            // 
            this.deleteDishBtn.Location = new System.Drawing.Point(1284, 46);
            this.deleteDishBtn.Name = "deleteDishBtn";
            this.deleteDishBtn.Size = new System.Drawing.Size(126, 43);
            this.deleteDishBtn.TabIndex = 8;
            this.deleteDishBtn.Text = "Delete selected Dish";
            this.deleteDishBtn.UseVisualStyleBackColor = true;
            this.deleteDishBtn.Click += new System.EventHandler(this.deleteDishBtn_Click);
            // 
            // webBrowser1
            // 
            this.webBrowser1.Location = new System.Drawing.Point(1230, 399);
            this.webBrowser1.MinimumSize = new System.Drawing.Size(20, 20);
            this.webBrowser1.Name = "webBrowser1";
            this.webBrowser1.Size = new System.Drawing.Size(450, 369);
            this.webBrowser1.TabIndex = 11;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1702, 799);
            this.Controls.Add(this.webBrowser1);
            this.Controls.Add(this.editVideoBtn);
            this.Controls.Add(this.addVideoGroupBox);
            this.Controls.Add(this.deleteDishBtn);
            this.Controls.Add(this.editDishBtn);
            this.Controls.Add(this.deleteVideoBtn);
            this.Controls.Add(this.videosLabel);
            this.Controls.Add(this.addDishGroupBox);
            this.Controls.Add(this.dishPictureBox);
            this.Controls.Add(this.searchTxtBox);
            this.Controls.Add(this.SearchBtn);
            this.Controls.Add(this.dataGridView2);
            this.Controls.Add(this.dataGridView1);
            this.Controls.Add(this.LoadDishesBtn);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dishPictureBox)).EndInit();
            this.addDishGroupBox.ResumeLayout(false);
            this.addDishGroupBox.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dataGridView2)).EndInit();
            this.addVideoGroupBox.ResumeLayout(false);
            this.addVideoGroupBox.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button LoadDishesBtn;
        private System.Windows.Forms.DataGridView dataGridView1;
        private System.Windows.Forms.Button SearchBtn;
        private System.Windows.Forms.TextBox searchTxtBox;
        private System.Windows.Forms.PictureBox dishPictureBox;
        private System.Windows.Forms.GroupBox addDishGroupBox;
        private System.Windows.Forms.Button addOrUpdateDishBtn;
        private System.Windows.Forms.TextBox ingridientsTxtBox;
        private System.Windows.Forms.TextBox imageUrlTxtBox;
        private System.Windows.Forms.TextBox cookingTimeTxtBox;
        private System.Windows.Forms.TextBox originTxtBox;
        private System.Windows.Forms.TextBox dishNametxtBox;
        private System.Windows.Forms.Label ingridientsLabel;
        private System.Windows.Forms.Label imageUrlLabel;
        private System.Windows.Forms.Label cookingTimeLabel;
        private System.Windows.Forms.Label OrginLabel;
        private System.Windows.Forms.Label dishNameLabel;
        private System.Windows.Forms.Label videosLabel;
        private System.Windows.Forms.DataGridView dataGridView2;
        private System.Windows.Forms.Button deleteVideoBtn;
        private System.Windows.Forms.Label dishIdLabel;
        private System.Windows.Forms.CheckBox editCheckBox1;
        private System.Windows.Forms.Button editDishBtn;
        private System.Windows.Forms.GroupBox addVideoGroupBox;
        private System.Windows.Forms.TextBox videoYearTxtBox;
        private System.Windows.Forms.TextBox videoLangTxtBox;
        private System.Windows.Forms.TextBox videoURLTxtBox;
        private System.Windows.Forms.TextBox videoTitleTxtBox;
        private System.Windows.Forms.Label videoDishIdLabel;
        private System.Windows.Forms.Label videoYearLabel;
        private System.Windows.Forms.Label videoLangLabel;
        private System.Windows.Forms.Label videoURLLabel;
        private System.Windows.Forms.Label videoTitleLabel;
        private System.Windows.Forms.Button editVideoBtn;
        private System.Windows.Forms.Button addOrUpdateVideoBtn;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button deleteDishBtn;
        private System.Windows.Forms.Label videoIdLabel;
        private System.Windows.Forms.CheckBox editCheckBox2;
        private System.Windows.Forms.WebBrowser webBrowser1;
    }
}

