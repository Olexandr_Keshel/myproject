﻿using System.Collections.Generic;

namespace MyProject
{
    internal class Video
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string VideoURL { get; set; }
        public string Language { get; set; }
        public int Year { get; set; }
        public int DishID { get; set; }


    }
}