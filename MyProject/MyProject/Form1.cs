﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MyProject
{
    public partial class Form1 : Form
    {
        BindingSource dishBindingSource = new BindingSource();
        BindingSource videosBindingSource = new BindingSource();
        

        List<Dish> dishes = new List<Dish>();

        public void LoadDishes() 
        {
            DishesDAO dishesDAO = new DishesDAO();

            //connect the list to the gridview 
            dishes = dishesDAO.GetAllDishes();

            dishBindingSource.DataSource = dishes;
            dataGridView1.DataSource = dishBindingSource;
            //connect the list to the list box
            comboBox1.DataSource = dishBindingSource;
        }

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            LoadDishes();
        }
        private void ReloadDishesBtn_Click(object sender, EventArgs e)
        {
            this.LoadDishes();
            searchTxtBox.Text = "";
        }
        private void SearchBtn_Click(object sender, EventArgs e)
        {
            DishesDAO dishesDAO = new DishesDAO();

            dishBindingSource.DataSource = dishesDAO.SearchTitles(searchTxtBox.Text);
            dataGridView1.DataSource = dishBindingSource;
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dataGridView = (DataGridView)sender;

            int rowClicked = dataGridView.CurrentRow.Index;

            string imageURL = dataGridView.Rows[rowClicked].Cells[4].Value.ToString(); ;
            
            dishPictureBox.Load(imageURL);
            
            videosBindingSource.DataSource = dishes[rowClicked].Videos ;
            dataGridView2.DataSource = videosBindingSource;


        }

        private void addOrUpdateDishBtn_Click(object sender, EventArgs e)
        {
            if(editCheckBox1.Checked == true)
            {
                int rowClicked = dataGridView1.CurrentRow.Index;
                Dish editDish = dishes[rowClicked];
                editDish.DishName = dishNametxtBox.Text;
                editDish.Origin = originTxtBox.Text;
                editDish.CookingTime = cookingTimeTxtBox.Text;
                editDish.ImageURL = imageUrlTxtBox.Text;
                editDish.Ingridients = ingridientsTxtBox.Text;

                DishesDAO dishesDAO = new DishesDAO();
                int result = dishesDAO.UpdateDish(editDish, editDish.ID);

                dishNametxtBox.Text = "";
                originTxtBox.Text = "";
                cookingTimeTxtBox.Text = "";
                imageUrlTxtBox.Text = "";
                ingridientsTxtBox.Text = "";
                dishIdLabel.Text = "dish ID";
                editCheckBox1.Checked = false;

                MessageBox.Show(result + " row(s) updated");

                LoadDishes();
            }
            else
            {
                Dish dish = new Dish
                {
                    DishName = dishNametxtBox.Text,
                    Origin = originTxtBox.Text,
                    CookingTime = cookingTimeTxtBox.Text,
                    ImageURL = imageUrlTxtBox.Text,
                    Ingridients = ingridientsTxtBox.Text
                };

                DishesDAO dishesDAO = new DishesDAO();
                int result  = dishesDAO.AddOneDish(dish);

                dishNametxtBox.Text = "";
                originTxtBox.Text = "";
                cookingTimeTxtBox.Text = "";
                imageUrlTxtBox.Text = "";
                ingridientsTxtBox.Text = "";
                dishIdLabel.Text = "dish ID";

                MessageBox.Show(result + " new row(s) inserted");

                LoadDishes();
            }

        }
        private void deleteDishBtn_Click(object sender, EventArgs e)
        {
            int rowClicked = dataGridView1.CurrentRow.Index;
            int dishID = (int)dataGridView1.Rows[rowClicked].Cells[0].Value;
            MessageBox.Show("ID of video " + dishID);

            DishesDAO dishesDAO = new DishesDAO();
            int result = dishesDAO.DeleteDish(dishID);
            MessageBox.Show(result + "row(s) deleted");

            LoadDishes();
        }

        private void editDishBtn_Click(object sender, EventArgs e)
        {
            int rowClicked = dataGridView1.CurrentRow.Index;


            Dish editDish = dishes[rowClicked];
            dishNametxtBox.Text = editDish.DishName.ToString();
            originTxtBox.Text = editDish.Origin.ToString();
            cookingTimeTxtBox.Text = editDish.CookingTime.ToString();
            imageUrlTxtBox.Text = editDish.ImageURL.ToString();
            ingridientsTxtBox.Text = editDish.Ingridients.ToString();

            editCheckBox1.Checked = true;
            dishIdLabel.Text = "dish ID: " + editDish.ID.ToString();
        }

        private void addOrUpdateVideoBtn_Click(object sender, EventArgs e)
        {
            if (editCheckBox2.Checked == true)
            {
                int rowClicked = dataGridView2.CurrentRow.Index;
                Dish dish = dishes.Find((el) => el.ID == (int)dataGridView2.Rows[rowClicked].Cells[5].Value);
                Video editVideo = dish.Videos[rowClicked];
                Dish selectedDish = (Dish)comboBox1.SelectedItem;
                editVideo.Title = videoTitleTxtBox.Text;
                editVideo.VideoURL = videoURLTxtBox.Text;
                editVideo.Language = videoLangTxtBox.Text;
                editVideo.Year = Convert.ToInt32(videoYearTxtBox.Text);
                editVideo.DishID = Convert.ToInt32(selectedDish.ID);

                DishesDAO dishesDAO = new DishesDAO();
                int result = dishesDAO.UpdateVideo(editVideo, editVideo.ID);

                videoTitleTxtBox.Text = "";
                videoURLTxtBox.Text = "";
                videoLangTxtBox.Text = "";
                videoYearTxtBox.Text = "";
                videoIdLabel.Text = "dish ID";
                editCheckBox2.Checked = false;

                MessageBox.Show(result + " row(s) updated");

                dataGridView2.DataSource = null;
                LoadDishes();
            }
            else
            {
                Dish selectedDish = (Dish)comboBox1.SelectedItem;
                Video newVideo = new Video
                {
                    Title = videoTitleTxtBox.Text,
                    VideoURL = videoURLTxtBox.Text,
                    Language = videoLangTxtBox.Text,
                    Year = Convert.ToInt32(videoYearTxtBox.Text),
                    DishID = Convert.ToInt32(selectedDish.ID)

                };
                DishesDAO dishesDAO = new DishesDAO();
                int result = dishesDAO.AddNewVideo(newVideo);
                MessageBox.Show(result + "new video(s) added");

                videoTitleTxtBox.Text = "";
                videoURLTxtBox.Text = "";
                videoLangTxtBox.Text = "";
                videoYearTxtBox.Text = "";
                videoIdLabel.Text = "dish ID";

                dataGridView2.DataSource = null;
                LoadDishes();
            }
        }

        private void editVideoBtn_Click(object sender, EventArgs e)
        {
            int rowClicked = dataGridView2.CurrentRow.Index;
            Dish dish = dishes.Find((el) => el.ID == (int)dataGridView2.Rows[rowClicked].Cells[5].Value);
            Video editVideo = dish.Videos[rowClicked];

            videoTitleTxtBox.Text = editVideo.Title.ToString();
            videoURLTxtBox.Text = editVideo.VideoURL.ToString();
            videoLangTxtBox.Text = editVideo.Language.ToString();
            videoYearTxtBox.Text = editVideo.Year.ToString();

            editCheckBox2.Checked = true;
            videoIdLabel.Text = "video ID: " + editVideo.ID.ToString();
        }

        private void deleteVideoBtn_Click(object sender, EventArgs e)
        {
            int rowClicked = dataGridView2.CurrentRow.Index;
            int videoID = (int)dataGridView2.Rows[rowClicked].Cells[0].Value;
            MessageBox.Show("ID of video " + videoID);

            DishesDAO dishesDAO = new DishesDAO();
            int result = dishesDAO.DeleteVideo(videoID);
            MessageBox.Show(result + "video(s) deleted");
            dataGridView2.DataSource = null;
            LoadDishes();
        }

        private void dataGridView2_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            DataGridView dataGridView = (DataGridView)sender;

            int rowClicked = dataGridView.CurrentRow.Index;

            string videoURL = dataGridView.Rows[rowClicked].Cells[2].Value.ToString();

            string html = "<html><head>";
            html += "<meta content='IE=Edge' http-equiv='X-UA-Compatible'/>";
            html += "<iframe id='video' src='https://www.youtube.com/embed/{0}' width='370' height='320' frameborder='0' allowfullscreen </iframe>";
            html += "</html></head>";

            webBrowser1.DocumentText = string.Format(html, videoURL.Split(new string[] { "youtu.be/" }, StringSplitOptions.None)[1]);
        }
    }
}
