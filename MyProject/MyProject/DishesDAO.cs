﻿using MySql.Data.MySqlClient;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace MyProject
{
    internal class DishesDAO
    {
        string connectionString = "datasource=localhost;port=3306;username=root;password=root;database=my cuisine;";

        public List<Dish> GetAllDishes()
        {
            //start with empty list
            List<Dish> returnThese = new List<Dish>();

            //connect to the mysql server
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open(); //log in to the server

            //define the sql statement to fetch all dishes
            MySqlCommand command = new MySqlCommand("SELECT `ID`, `DISH_NAME`, `PLACE_OF_ORIGIN`, `COOKING_TIME`, `IMAGE_NAME`, `INGREDIENTS` FROM DISHES", connection);

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Dish dish = new Dish
                    {
                        ID = reader.GetInt32(0),
                        DishName = reader.GetString(1),
                        Origin = reader.GetString(2),
                        CookingTime = reader.GetString(3),
                        ImageURL = reader.GetString(4),
                        Ingridients = reader.GetString(5)
                    };
                    dish.Videos = GetVideosOfDish(dish.ID);
                    returnThese.Add(dish);
                }
            }
            connection.Close();

            return returnThese;
        }

        public List<Dish> SearchTitles(string searchTerm)
        {
            List<Dish> returnThese = new List<Dish>();

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            string searchPhrase = "%" + searchTerm + "%";

            MySqlCommand command = new MySqlCommand();
            command.CommandText = "SELECT * FROM DISHES WHERE DISH_NAME LIKE @search";
            command.Parameters.AddWithValue("@search", searchPhrase);
            command.Connection = connection;

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Dish dish = new Dish
                    {
                        ID = reader.GetInt32(0),
                        DishName = reader.GetString(1),
                        Origin = reader.GetString(2),
                        CookingTime = reader.GetString(3),
                        ImageURL = reader.GetString(4),
                        Ingridients = reader.GetString(5)
                    };
                    returnThese.Add(dish);
                }
            }
            connection.Close();

            return returnThese;
        }

        internal int AddOneDish(Dish dish)
        { 
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open(); 

            MySqlCommand command = new MySqlCommand("INSERT INTO `dishes`(`DISH_NAME`, `PLACE_OF_ORIGIN`, `COOKING_TIME`, `IMAGE_NAME`, `INGREDIENTS`) VALUES (@dishname, @origin,@cookingtime,@imageURL,@ingridients)", connection);
            command.Parameters.AddWithValue("@dishname", dish.DishName);
            command.Parameters.AddWithValue("@origin", dish.Origin);
            command.Parameters.AddWithValue("@cookingtime", dish.CookingTime);
            command.Parameters.AddWithValue("@imageURL", dish.ImageURL);
            command.Parameters.AddWithValue("@ingridients", dish.Ingridients);

            int newRows = command.ExecuteNonQuery();


            connection.Close();

            return newRows;
        }

        internal int UpdateDish(Dish dish, int dishId)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand command = new MySqlCommand("UPDATE `dishes` SET `DISH_NAME` = @dishname, `PLACE_OF_ORIGIN` = @origin, `COOKING_TIME` = @cookingtime, `IMAGE_NAME` = @imageURL, `INGREDIENTS` = @ingridients WHERE dishes.ID = @dishid;", connection);
            command.Parameters.AddWithValue("@dishname", dish.DishName);
            command.Parameters.AddWithValue("@origin", dish.Origin);
            command.Parameters.AddWithValue("@cookingtime", dish.CookingTime);
            command.Parameters.AddWithValue("@imageURL", dish.ImageURL);
            command.Parameters.AddWithValue("@ingridients", dish.Ingridients);
            command.Parameters.AddWithValue("@dishid", dishId);

            int rowsUpdated = command.ExecuteNonQuery();


            connection.Close();

            return rowsUpdated;
        }
        internal int DeleteDish(int dishID)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand command = new MySqlCommand("DELETE FROM dishes WHERE dishes.ID = @dishid", connection);
            command.Parameters.AddWithValue("@dishid", dishID);

            int result = command.ExecuteNonQuery();

            connection.Close();

            return result;
        }

        public List<Video> GetVideosOfDish(int dishID)
        {
            List<Video> returnThese = new List<Video>();

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();


            MySqlCommand command = new MySqlCommand();
            command.CommandText = "SELECT * FROM VIDEOS WHERE dishes_ID = @dishid";
            command.Parameters.AddWithValue("@dishid", dishID);
            command.Connection = connection;

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    Video video = new Video
                    {
                        ID = reader.GetInt32(0),
                        Title = reader.GetString(1),
                        VideoURL = reader.GetString(2),
                        Language = reader.GetString(3),
                        Year = reader.GetInt32(4),
                        DishID = reader.GetInt32(5)
                    };
                    returnThese.Add(video);
                }
            }
            connection.Close();

            return returnThese;
        }

        public List<JObject> GetVideosUsingJoin(int dishID)
        {
            List<JObject> returnThese = new List<JObject>();

            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();


            MySqlCommand command = new MySqlCommand();
            command.CommandText = "SELECT videos.ID as VideoID, dishes.DISH_NAME ,`video_title`, `video_URL`, `video_language`, `video_year` FROM `videos` JOIN dishes ON videos.dishes_ID = dishes.ID where dishes.ID = @dishid";
            command.Parameters.AddWithValue("@dishid", dishID);
            command.Connection = connection;

            using (MySqlDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    JObject newVideo = new JObject();

                    for(int i = 0; i < reader.FieldCount; i++)
                    {
                        newVideo.Add(reader.GetName(i).ToString(), reader.GetValue(i).ToString()); ;
                    }

                    returnThese.Add(newVideo);
                }
            }
            connection.Close();

            return returnThese;
        }

        internal int DeleteVideo(int videoID)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand command = new MySqlCommand("DELETE FROM videos WHERE videos.ID = @videoid", connection);
            command.Parameters.AddWithValue("@videoid", videoID);
            

            int result = command.ExecuteNonQuery();


            connection.Close();

            return result;
        }

        internal int AddNewVideo(Video newVideo)
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand command = new MySqlCommand("INSERT INTO videos(`video_title`, `video_URL`, `video_language`, `video_year`, `dishes_ID`) VALUES (@videotitle, @videoURL, @videolaguage, @videoyear, @dishesid)", connection);
            command.Parameters.AddWithValue("@videotitle", newVideo.Title);
            command.Parameters.AddWithValue("@videoURL", newVideo.VideoURL);
            command.Parameters.AddWithValue("@videolaguage", newVideo.Language);
            command.Parameters.AddWithValue("@videoyear", newVideo.Year);
            command.Parameters.AddWithValue("@dishesid", newVideo.DishID);

            int newRows = command.ExecuteNonQuery();


            connection.Close();

            return newRows;
        }

        internal int UpdateVideo(Video editVideo, int editVideoId) 
        {
            MySqlConnection connection = new MySqlConnection(connectionString);
            connection.Open();

            MySqlCommand command = new MySqlCommand("UPDATE `videos` SET `video_title`=@videotitle,`video_URL`=@videoURL,`video_language`=@videolanguage,`video_year`=@videoyear,`dishes_ID`=@videoDishid WHERE videos.ID = @videoid", connection);
            command.Parameters.AddWithValue("@videotitle", editVideo.Title);
            command.Parameters.AddWithValue("@videoURL", editVideo.VideoURL);
            command.Parameters.AddWithValue("@videolanguage", editVideo.Language);
            command.Parameters.AddWithValue("@videoyear", editVideo.Year);
            command.Parameters.AddWithValue("@videoDishid", editVideo.DishID);
            command.Parameters.AddWithValue("@videoid", editVideoId);

            int rowsUpdated = command.ExecuteNonQuery();


            connection.Close();

            return rowsUpdated;
        }
    }
}
    
    

