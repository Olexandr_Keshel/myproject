﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MyProject
{
    internal class Dish
    {
        public int ID {  get; set; }
        public string DishName { get; set; }
        public string Origin { get; set;}
        public string CookingTime { get; set; }
        public string ImageURL { get; set; }
        public string Ingridients { get; set; }
        public List<Video> Videos { get; set; }

        public override string ToString()
        {
            return this.ID.ToString() + " " + this.DishName.ToString();
        }
    }
}
